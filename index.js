



// Bài_1:
function Bai1() {
    var n = 0;
    var sum = 0;
    while (sum <= 10000) {
        sum = n * (n + 1) / 2;
        n++;
    }
    document.getElementById("ketQua1").innerText = `Số nguyên n nhỏ nhất là : ${n - 1}`

}
// Bài_2:
function Bai2() {
    var x = document.getElementById("soX").value * 1;
    var n = document.getElementById("soN").value * 1;
    if (x <= 0 || n <= 0) {
        document.getElementById("ketQua2").innerText = `Xin nhập số lớn hơn 0`

    } else {
        var sum = 0;
        var k = 1;
        while (k <= n) {
            sum = sum + Math.pow(x, k)
            k++;
        }
        document.getElementById("ketQua2").innerText = `Tổng là ${sum}`
    }
}
//Bài_3
function Bai3() {
    var n = document.getElementById("soGiaiThua").value * 1;
    var sum = 1;
    if (n <= 0) {
        alert("Xin nhập số lớn hơn 0")

    } else {
        for (var k = 1; k <= n; k++) {
            sum = sum * k;
        }
        document.getElementById("ketQua3").innerText = `${n}! là  ${sum}`

    }
}
//Bài_4
function Bai4() {
    var n = 1;
    var output = "";
    while (n <= 10) {
        if (n % 2 == 0) {
            output += `<h5 style="background-color:red;color: white;text-align:center">Div chẵn ${n}</h5>`

        } else {
            output += `<h5 style="background-color:blue;color: white;text-align:center">Div lẻ ${n}</h5>`

        }
        ; n++;

    }
    document.getElementById("ketQua4").innerHTML = output;
}



